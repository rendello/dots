alias vim="nvim"
alias acquire="sudo apt install"
alias l="ls -al"
alias brt="xrandr --output eDP-1 --brightness"
alias alsamixer="alsamixer -g"
alias "cd.."="cd .."
alias "cd-"="cd -"
alias "cd~"="cd"
alias old_glog='git --no-pager log --reverse --pretty="%C(green)№ %h×%C(reset)%ar×%C(green)¦×%C(cyan)%s" | column -t -s ×'


mkcd () {
	mkdir "$1"
	cd "$1"
}

cd ~
