
" VUNDLE STUFF
set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()



" VUNDLE PACKAGES
Plugin 'gmarik/Vundle.vim' " Required
Plugin 'hail2u/vim-css3-syntax'
Plugin 'skammer/vim-css-color'
Plugin 'tmhedberg/SimpylFold'
Plugin 'junegunn/vim-easy-align'
Plugin 'chrisbra/unicode.vim'
Plugin 'tbastos/vim-lua'
Plugin 'SirVer/ultisnips'


" CALLING VUNDLE PACKAGES
call vundle#end()
filetype plugin indent on



" PLUGIN SETTINGS

" --> ULTINSNIPS
"let g:UltiSnipsSnippetDirectories=["UltiSnips"]
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-b>"
let g:UltiSnipsJumpBackwardTrigger="<c-z>"
let g:UltiSnipsEditSplit="vertical"


" GENERAL STARTUP
filetype on
set mouse=n " Lets you drag to resize splits
set autoindent
set shiftwidth=4
set tabstop=4
set relativenumber
set cursorline
set foldmethod=syntax
set ttimeoutlen=100
set nolazyredraw
set guicursor= " Fixes artifacts in Terminator

if !has('gui_running')
	set t_Co=256
endif

autocmd GuiEnter * set background&



" GENERAL REMAPS
nmap j jzz
nmap k kzz
nnoremap ; :
nnoremap : ;



" LEADER STUFF
let mapleader = "\<Space>"

vmap <Leader>y "+y
vmap <Leader>d "+d
nmap <Leader>p "+p
nmap <Leader>P "+P
vmap <Leader>p "+p
vmap <Leader>P "+P
map <Leader>l ;set cursorline!<CR>

nmap <Leader><Leader> V


" Go to tab by number
noremap <A-1> 1gt
noremap <A-2> 2gt
noremap <A-3> 3gt
noremap <A-4> 4gt
noremap <A-5> 5gt
noremap <A-6> 6gt
noremap <A-7> 7gt
noremap <A-8> 8gt
noremap <A-9> 9gt
noremap <A-0> :tablast<cr>
